
### Project Name: 
  Mileage

### Team Name: 
  Mileage
   
  Contributors: 
  - Robert Moir
  - Jessica Radley
  - Barry Rowe

### Short Project Description

A DApp to support carbon negative driving and prevent odometer fraud.

### Long Project Description

A protoype of a system that provides immutable proof of mileage driven used to distribute loyalty points and offset tailpipe carbon based on the actual distance driven by the vehicle.

For the prototype, there are four steps to the process:
1. Drive a certain distance (in units of 5 miles)
2. Sign the odometer state
3. Submit this signature to trigger three things:
    1. Writing the signed odometer state to the blockchain for immutability
    2. The car company burns a sufficient number of micro carbon credits to offset the carbon generated over the distance travelled since the last signature submission
    3. The car company sends you a number of loyalty points that correspond to the distance travelled since the last submission
4. (Optional) Send your loyalty points to another address

For the full POC, these steps will all be automated by a mobile app that collects data from the car to provide incontrovertible evidence of mileage that is then signed and stored on chain. This mobile app will also enable the loyalty points to be converted to cash so that it can be spent using traditional payment rails.

### Tech Stack

  - Web3.js
  - Solidity 
  - Remix
  - Python 
  - Tornado Web

### Payment Address

0x50D7b0aDfd593B8972d6bDcE974E43d37653e431

### Project Links:

- Demo: https://dapp.satoserver.com/milkomeda/mileage
- Git: https://gitlab.com/spherebeaker/milkomeda-mileage

### Documentation on how to run the project

Before running the project you will need to install MetaMask in your favourite brower and follow the setup instructions provided at the following link to configure MetaMask to work with Milkomeda:

https://dcspark.github.io/milkomeda-documentation/cardano/for-developers/configuring-metamask-for-devs

You will also need testnet milkTADA to your wallet to use the DApp, which you can acquire from this faucet:

https://faucet-devnet-cardano-evm.c1.milkomeda.com/

You can then run the project either by going to the demo website linked above, or by following these steps:

1. Clone the repo from the git link above
2. In a terminal run `pip install -r req.txt`
3. Then run `python serve.py`
4. Go to http://localhost:6440/milkomeda/mileage

### Contracts

- carboncredits.sol (`mileage/contracts/carboncredits.sol`)
- miles.sol (`mileage/contracts/miles.sol`)

### Frontend code

-  See `/mileage/static/`

### 11. Backend code:

- See `/mileage/serve.py`

### 12. Screens / graphic materials (optional)

### 13. Project social media links (if applicable)

### 14. Recorded pitch (please send it also to iwo@milkomeda.com)
Pitch: https://www.youtube.com/watch?v=XKtRhT8g844

