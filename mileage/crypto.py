import web3
from eth_account.messages import encode_defunct


rpc_url = "https://rpc-devnet-cardano-evm.c1.milkomeda.com"
chain_id = 200101

key = open("local.key").read().strip()
address = "0x2b9ae811E5115E28C328D240C5c56bc4De085414"
w3 = web3.Web3()


class Signer:
    @classmethod
    def sign_message(cls, driver, amount, nonce):
        address2 = w3.toChecksumAddress(driver)
    
        msghash = w3.solidityKeccak(['address', 'uint256', 'uint256'], [address2, amount, nonce])
       
        sig = w3.eth.account.signHash(msghash, key)
        sig_hex = sig.signature.hex()
        
        return [msghash.hex(), driver, sig_hex, amount, nonce]

    @classmethod
    def verify_message(cls, address, sig, message):
        msg_signer = w3.eth.account.recover_message(encode_defunct(text=message), signature=sig)
        addr, miles, nonce = message.split("_")
       
        if msg_signer.lower() == address.lower():
            if addr.lower() == address.lower():
               return True
        return False


def main():
    address = "0x6783da1ba5d80e77e774c3d89fbdd86af44723f0"
    sig = "0x21664c221dd5c4653a8193a98f1b0a4bc83124b310c64b1887500f0b4db937963abf8bfacd4368aa94a94c402f956393ff5e29b6b72be49432b8fa04912759111b"
    message = "asd0aggag"
    w3.eth.account.recover_message(encode_defunct(text="asd0aggag"), signature=sig)
    Signer.verify_message(address, sig, message)
  
if __name__=='__main__':
    main()

