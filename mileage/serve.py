import tornado.web
import tornado.ioloop
import crypto
import json
import random

class MileageHandler(tornado.web.RequestHandler):
    def get(self):
        html = open("./static/html/mileage.html").read()
        self.write(html)


class SignerHandler(tornado.web.RequestHandler):
    def post(self):
        body = json.loads(self.request.body)
        sig = body['sig']
        address = body['address']
        amount = body['amount']
        nonce1 = body['nonce']
        nonce2 = int(random.random()*1000000000)
        message1 = address+"_"+str(amount)+"_"+nonce1
        #verify incoming sign
        verify = crypto.Signer.verify_message(address, sig, message1)
        if verify:
            out = crypto.Signer.sign_message(address, amount, nonce2)
            rval =  {"output": out, "msghash": out[0], "driver": out[1], "sig": out[2], "amount": out[3], "nonce": out[4]}
            self.write(json.dumps(rval))
        else:
            self.write(json.dumps({"error": "invalid signature"}))


def main():
    app = tornado.web.Application([
      (r'/milkomeda/mileage', MileageHandler), 
      (r'/milkomeda/signer', SignerHandler),
      (r'/milkomeda/static/(.*)', tornado.web.StaticFileHandler,
          {'path': "./static"})
    ])
    app.listen(6440)
    tornado.ioloop.IOLoop.current().start()


if __name__=="__main__":
    main()

