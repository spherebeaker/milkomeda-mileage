
var odometer = 0;
var posted_miles = 0
var address = ""
var balance = 0

var posted_sign = ""

async function drive(num) {
  odometer += num;
  await refreshApp();
}


async function getSig() {
  //create request object and sign it.
  var amount = odometer
  var body = {"sig": "", 
              "address": address,
              "amount": odometer,
              "nonce": (Math.random()).toString()
             }
  var nonce = body['nonce'];
  var message = address+"_"+amount+"_"+nonce;
  body['message'] = message
  //bops = safe_buffer
  //var msg = `0x${.from(message, 'utf8').toString('hex')}`;
  var msg = '0x'+safe_buffer.Buffer.from(message, 'utf-8').toString('hex');

  const sig = await ethereum.request({
    method: 'personal_sign',
    params: [msg, address],
  });
  body['sig'] = sig
  //send off to server to sign it
  //get the signature back.
  posted_sign = await sendRequest("POST", "/milkomeda/signer", JSON.stringify(body))
  posted_sign = JSON.parse(posted_sign);
  await refreshApp();
}


async function submitSig() {
    var out = posted_sign['output'];
    var rr = await mileage.submitHash(out[0],out[1],out[2],out[3],out[4]);

    await rr.wait();
    await refreshApp();
}

async function sendCurrency() {
    var to = document.getElementById("send_address").value;
    var amount = document.getElementById("send_amount").value;
    var rr = await mileage.transfer(to, amount);
    await rr.wait();
    refreshApp();
}

async function refreshApp() {
  //load various data stuffs and whatnot.
  //get address
  address = ethereum.selectedAddress;
  console.log("JJJJJJJJJJ")
  balance = parseInt((await mileage.balanceOf(address)).toString());
  console.log(balance, odometer)
  miles = parseInt((await mileage.getMiles(address)).toString());
  if (miles > odometer) {
    odometer = miles;   
  }
  var elm1 = document.getElementById("address")
  elm1.innerText = address;
 
  var elm2 = document.getElementById("odometer")
  elm2.innerText = odometer;
 
  var elm3 = document.getElementById("posted_miles")
  elm3.innerText = miles;
 
  var elm4 = document.getElementById("balance")
  elm4.innerText = balance;

  var elm5 = document.getElementById("signed_miles")
  if (posted_sign)
    var signed_miles = posted_sign['output'][3];
  else
    var signed_miles = "";
  elm5.innerText = signed_miles;
 

}





//utils: 

function sendRequest(method, url, body) {
  return new Promise(function(resolve, reject) {
    var http = new XMLHttpRequest();
    var url=url;
    http.open(method, url);
    if (body)
      http.send(body);
    else
      http.send();

    http.onreadystatechange = function(e) {
      if (this.readyState == 4 && this.status == 200)
          resolve(http.responseText);
    }
  });
}




// Messy ethereum/web3 code now:

const { ethereum } = window;

async function onPageLoad() {
  console.log("99999999999999999999")
  if (Boolean(ethereum && ethereum.isMetaMask)) {
    loadContracts();
    if (!ethereum.isConnected() || !ethereum.selectedAddress) {
      console.log(888888888);
      var elm = document.getElementById("connectMetaMask");
      elm.onclick = clickMetaMask;
      hideDapp();

    } else {
     console.log(1111)
     await showDapp();
    }
  } else {
    setConnectMessage("MetaMask is not installed!", true);
  }
}

async function clickMetaMask() {
 try {
    // Will open the MetaMask UI
    // You should disable this button while the request is pending!
    ethereum.request({ method: 'eth_requestAccounts' }).then(function(x){
      console.log("wallet connected");
      showDapp();
    });
  } catch (error) {
    console.error(error);
  }
}



async function showDapp() {
  var elm1 = document.getElementById("metamask-frame");
  var elm2 = document.getElementById("dapp");
  elm1.style.display = "none";
  elm2.style.display = "block";
  await refreshApp();
}


function hideDapp() {
  var elm1 = document.getElementById("metamask-frame");
  var elm2 = document.getElementById("dapp");

  elm1.style.display = "block";
  elm2.style.display = "none";
}

var contract_address = "0xB3C59Ead85bA76bf3b39B30dDe9116a8b4e9CcEf";
var mileage;

function loadContracts() {
  // A Web3Provider wraps a standard Web3 provider, which is
  // what Metamask injects as window.ethereum into each page
  gProvider = new _ethers.providers.Web3Provider(window.ethereum)

  // The Metamask plugin also allows signing transactions to
  // send ether and pay to change state within the blockchain.
  // For this, you need the account signer...
  gSigner = gProvider.getSigner()


  var mileage_contract = new _ethers.Contract(
                                       contract_address,
                                       CONTRACT_ABI,
                                       gProvider);
  mileage = mileage_contract.connect(gSigner);
}







// Helper functions....

function _base64ToArrayBuffer(base64) {
  var base64_string = base64.substr(base64.indexOf("base64,")+7, base64.length);
  var binary_string = window.atob(base64_string);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}



async function sha256(message) {
  // encode as UTF-8
  const msgBuffer = new TextEncoder().encode(message);
  return sha256_bin(msgBuffer);
}

async function sha256_bin(msgBuffer) {
  // hash the message
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);

  // convert ArrayBuffer to Array
  const hashArray = Array.from(new Uint8Array(hashBuffer));

  // convert bytes to hex string                  
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
  return hashHex;
}

function isHex(h) {
var a = parseInt(h,16);
return (a.toString(16) === h)
}


function hexStringToByteArray(hexString) {
    if (hexString.length % 2 !== 0) {
        throw "Must have an even number of hex digits to convert to bytes";
    }/* w w w.  jav  a2 s .  c o  m*/
    var numBytes = hexString.length / 2;
    var byteArray = new Uint8Array(numBytes);
    for (var i=0; i<numBytes; i++) {
        byteArray[i] = parseInt(hexString.substr(i*2, 2), 16);
    }
    return byteArray;
}

const escapeHTML = (unsafe) => {
    return unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
}



function copyToClipboard (str) {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
};

function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

function sendRequest(method, uri, body) {
  return new Promise(function(resolve, reject) {
    var http = new XMLHttpRequest();
    var url=uri;
    http.open(method, url);
    if (body)
      http.send(body);
    else
      http.send();

    http.onreadystatechange = function(e) {
      if (this.readyState == 4 && this.status == 200)
          resolve(http.responseText);
    }
  });
}
