from setuptools import setup, find_packages

__VERSION__ = "1.0.0"

def main(args=None):
    README = open("./README").read()

    setup_required_packages = []

    required_packages = ["pymongo<4.0", "tornado", 
                         "crypto", "pycrypto",
                         "numpy", "pillow", "ecdsa",
                         "base58", "six", "python-dateutil",
                         "web3"
                        ]

    test_required_packages = ["nose", "coverage"]

    settings = dict(name="mileage",
                    version=__VERSION__,
                    description="mileage",
                    long_description=README,
                    classifiers=["Programming Language :: Python", ],
                    author="",
                    author_email="",
                    url="",
                    keywords="mileage",
                    packages=find_packages(),
                    include_package_data=True,
                    zip_safe=False,
                    install_requires=required_packages,
                    tests_require=test_required_packages,
                    test_suite="nose.collector",
                    setup_requires=setup_required_packages,
                    entry_points="""\
                        [console_scripts] 
                        lt_serve=lt_timestamp.serve:main
                        """,
                    )
    if args:
        settings['script_name'] = __file__
        settings['script_args'] = args
    setup(**settings)


if __name__ == "__main__":
    main()
